#  Dark Crystal Key Backup Protocol Specification

## 1. Introduction

Dark Crystal is a social key management system.  It is a set of protocols and recommendations for responsibly handling sensitive data such as secret keys. 

It is designed for safeguarding data which we don't want to loose, but which we don't want others to find. 

The idea is that rather than creating a generalised piece of software for managing keys, key management techniques should be integrated into the applications which use the keys. So the techniques described and libraries provided can be seen as recommendations for developers wanting to improve key management for particular applications.  Of course, every situation is different and in many cases the protocol will need to be adapted for particular needs.

This document describes a social key backup and recovery technique, to enable lost keys, passwords or other sensitive data to be recovered, using a small group of trusted contacts.

It must be emphasised that key recovery cannot solve the problem of compromised keys. It is appropriate only to recover encrypted data following loss of a key, or for continued use of a key when it is known to be lost but not compromised, for example following accidental deletion or hardware failure. 

## 2. Terms used

- ***Secret*** - the data to be backed up and potentially recovered.
- ***Secret-owner*** - the peer to whom the data belongs and who initiates the backup.
- ***Shard*** - a single encrypted share of the secret. 
- ***Custodian*** - a peer who holds a shard, generally a friend or trusted contact of the secret owner.

## 3. Key Loss Scenarios

These scenarios will be discussed in greater depth in the report on social factors. But it is important to already make the distinction between the three main kinds of key loss:

- ***'Swim'*** - key loss - For example, the computer fell into the sea and is lost forever.  In the case the key is assumed lost, but not compromised.
- ***'Theft'*** - key compromise - For example, I forgot my computer on a train and have no idea who might have it.  The key is both lost and compromised.
- ***'Inheritance'*** - following death, incapacitation or imprisonment, a key can be recovered by heirs.  This covers any situation where custodians recover the secret without the involvement of the secret owner.

## 4. Who is this for?

This protocol is designed to be integrated into applications which deal with sensitive data or where effective key management is critical.

The protocol is agnostic to transport and storage mechanisms, meaning messages used for this backup system can be transmitted and stored in the same way your application handles other types of data. The benefit of this is that it should be easy to integrate, and not require too many additional dependencies.

However, when deciding whether this protocol is a good fit for your application, several factors need to be taken into account. These are discussed in more detailed in our 'Threat modelling' and 'Social factors' reports.

The architecture of the application has an effect on how effective these techniques are.  This backup technique is robust because of its distributed nature. Ideally, shards are stored in multiple locations controlled by multiple 'custodians'. If the application uses a traditional client-server architecture, there is a concern that all shards are stored on the same server, meaning the same physical location.

This is perhaps be not as bad as it sounds, since each shard would be encrypted to a particular custodian, and the custodian's secret key should be stored only on their client device. But it is far from ideal, as a compromised server would mean a metadata leak as well as a possibility that the shards could be lost.

So while it is possible, though not advised, to use a centralised server to coordinate communication between peers, it must be emphasised that shards must be stored locally on client devices.  Reliance on a particular server is analogous to "putting all your eggs in one basket", and detracts from the core principle of this protocol, which is to provide a distributed backup. 

## 5. Peer experience

This is a technical specification of the protocol. Many stages of the process will be automated and not visible to the peers.  Interface recommendations and peer stories will be described elsewhere.

## 6. Cryptographic primitives

### 6.1 Introduction

Standards are generally adopted from the [Networking and Cryptography library (NaCl)](https://nacl.cr.yp.to/). These were chosen because they are widely adopted and regarded to be secure, and available in a wide variety of high level languages, either through bindings to the C library ['libsodium'](https://doc.libsodium.org/) (which is based on NaCl), or by high level implementations which are based on the same cryptographic parameters and algorithms and can reproduce the same behaviour in tests.

### 6.2 Notation used

![concatonation](./assets/concatonation.png)

Concatonation of a and b

![scalar multiplication](./assets/scalar_multiplication.png)

Deriving a shared key from keys a and b by scalar multiplication 

![box](./assets/box.png)

Authenticated encryption of message with key

![HMAC](./assets/hmac.png)

Keyed hash of message 

### 6.3 Authenticated Symmetric Encryption

The symmetric encryption algorithm used is NaCl's 'secretbox', which consists of the `XSalsa20` stream cipher, and a `poly1305` message authentication code, both developed by Daniel J. Birnstein.  These were chosen because they are widely adopted and regarded to be secure.

We encode encrypted messages as the nonce concatonated with the MAC, concatonated with the ciphertext:

![nonce MAC ciphertext](./assets/nonce_mac_ciphertext.png)

Since the nonce used by XSalsa20 is 24 bytes and the MAC used by poly1306 is 16 bytes, ciphertext messages are `plaintext length + 24 + 16` bytes long.

- [`KeyBackupCrypto.encrypt` in API documentation](https://dark-crystal-java.gitlab.io/dark-crystal-key-backup-java-docs/dark-crystal-key-backup-crypto-java/org/magmacollective/darkcrystal/keybackup/crypto/KeyBackupCrypto.html#encrypt(byte%5B%5D,byte%5B%5D))
- [`KeyBackupCrypto.decrypt` in API documentation](https://dark-crystal-java.gitlab.io/dark-crystal-key-backup-java-docs/dark-crystal-key-backup-crypto-java/org/magmacollective/darkcrystal/keybackup/crypto/KeyBackupCrypto.html#decrypt(byte%5B%5D,byte%5B%5D))
- [`KeyBackupCrypto.secretBox` (used internally by `encrypt`) in source code](https://gitlab.com/dark-crystal-java/dark-crystal-key-backup-crypto-java/-/blob/70f663ea2520c1a7aeb03fd2c0d1092ebcc028ed/src/main/java/org/magmacollective/darkcrystal/keybackup/crypto/KeyBackupCrypto.java#L191)

#### 6.3.1 External documentation links:

- [Daniel J. Bernstein 2005, Salsa20 Specification](https://cr.yp.to/snuffle/spec.pdf)
- [Daniel J. Bernstein, "Cryptography in NaCl" - formal specification](https://cr.yp.to/highspeed/naclcrypto-20090310.pdf)
- [nacl secretbox documentation](http://nacl.cr.yp.to/secretbox.html)
- [libsodium secretbox documentation](https://download.libsodium.org/doc/secret-key_cryptography/secretbox) - libsodium is a compatible fork of NaCl, with extended functionality.

### 6.4 Signing and Verification

Signing is achieved using the Edwards curve digital signature algorithm (EdDSA), using recommended parameters for the Ed25519 and Ed448 elliptic curves.

- [EdDSA in API documentation](https://dark-crystal-java.gitlab.io/dark-crystal-key-backup-java-docs/dark-crystal-key-backup-crypto-java/org/magmacollective/darkcrystal/keybackup/crypto/EdDSA.html)
- [EdDSA in source code](https://gitlab.com/dark-crystal-java/dark-crystal-key-backup-crypto-java/-/blob/70f663ea2520c1a7aeb03fd2c0d1092ebcc028ed/src/main/java/org/magmacollective/darkcrystal/keybackup/crypto/EdDSA.java)

#### 6.4.1 External documentation links:

- [Edwards-Curve Digital Signature Algorithm (EdDSA) - IETF RFC8032](https://tools.ietf.org/html/rfc8032)
- [ed25519 23pp. Daniel J. Bernstein, Niels Duif, Tanja Lange, Peter Schwabe, Bo-Yin Yang. High-speed high-security signatures. Journal of Cryptographic Engineering 2 (2012), 77–89.](https://cr.yp.to/papers.html#ed25519)
- [NaCl's `crypto_sign/ed25519`](https://ed25519.cr.yp.to/software.html)
- Java implementation used: [net.i2p.crypto:eddsa](https://mvnrepository.com/artifact/net.i2p.crypto/eddsa/0.3.0) [ed25519-java github repository](https://github.com/str4d/ed25519-java) - this has undergone two independent reviews, and the unit tests include tests against data from the python implementation.

### 6.5 Public Key Encryption

An agreement (shared secret) is calculated using Curve25519 keypairs. Both public and private keys are 32 bytes long.

Because of the way scalar multiplication works, the space of possible DH agreements is smaller than the space of possible keys, that is to say, there exist several sets of public and private keys which will give the same DH agreement.  To address this problem, we 'hash in' both parties' public keys to increase the 'uniqueness' of the shared secret.

Optionally, additional contextual information known by both parties can also be concatonated to the public keys and 'hashed in' to the secret, providing additional forward secrecy.

The shared secret is calculated by both parties as:

![shared secret](./assets/shared_secret.png)

Authenticated encryption is then achieved as described above, using the Xsalsa20 stream cipher with poly1305 MAC (secretbox).

- Java implementation of Curve25519 [org.whispersystems:curve25519](https://mvnrepository.com/artifact/org.whispersystems/curve25519-java/0.5.0) - this provides both a native implementation using JNI, and a pure Java 7 implementation.
- [`KeyBackupCrypto.box` in API documentation](https://dark-crystal-java.gitlab.io/dark-crystal-key-backup-java-docs/dark-crystal-key-backup-crypto-java/org/magmacollective/darkcrystal/keybackup/crypto/KeyBackupCrypto.html#box(byte%5B%5D,org.whispersystems.curve25519.Curve25519KeyPair,byte%5B%5D,byte%5B%5D))
- [`KeyBackupCrypto.unbox` in API documentation](https://dark-crystal-java.gitlab.io/dark-crystal-key-backup-java-docs/dark-crystal-key-backup-crypto-java/org/magmacollective/darkcrystal/keybackup/crypto/KeyBackupCrypto.html#unbox(byte%5B%5D,org.whispersystems.curve25519.Curve25519KeyPair,byte%5B%5D,byte%5B%5D))
- [`KeyBacupCrypto.box` in source code](https://gitlab.com/dark-crystal-java/dark-crystal-key-backup-crypto-java/-/blob/70f663ea2520c1a7aeb03fd2c0d1092ebcc028ed/src/main/java/org/magmacollective/darkcrystal/keybackup/crypto/KeyBackupCrypto.java#L97)
- [`KeyBacupCrypto.unbox` in source code](https://gitlab.com/dark-crystal-java/dark-crystal-key-backup-crypto-java/-/blob/70f663ea2520c1a7aeb03fd2c0d1092ebcc028ed/src/main/java/org/magmacollective/darkcrystal/keybackup/crypto/KeyBackupCrypto.java#L127)

### 6.6 Hashing

Hashing is done using a Blake2b keyed hash, with a digest length of 32 bytes unless specified otherwise.

- [`KeyBackupCrypto.blake2b` in API documentation (with key given)](https://dark-crystal-java.gitlab.io/dark-crystal-key-backup-java-docs/dark-crystal-key-backup-crypto-java/org/magmacollective/darkcrystal/keybackup/crypto/KeyBackupCrypto.html#blake2b(byte%5B%5D,byte%5B%5D))
- [`KeyBackupCrypto.blake2b` in API documentation (without key)](https://dark-crystal-java.gitlab.io/dark-crystal-key-backup-java-docs/dark-crystal-key-backup-crypto-java/org/magmacollective/darkcrystal/keybackup/crypto/KeyBackupCrypto.html#blake2b(byte%5B%5D))
- [`KeyBackupCrypto.blake2b` in source code](https://gitlab.com/dark-crystal-java/dark-crystal-key-backup-crypto-java/-/blob/70f663ea2520c1a7aeb03fd2c0d1092ebcc028ed/src/main/java/org/magmacollective/darkcrystal/keybackup/crypto/KeyBackupCrypto.java#L68)

### 6.7 Key derivation

Curve25519 (encryption) keys can be derived from Ed25519 (signing) keys, in such a way that both secret and public components can be derived, meaning we only need to establish a signing keypair.  This is very convenient as it reduces the amount of keys we need to manage, but it is regarded by many cryptographers as bad practice and is not recommended if it can be avoided.

## 7. Higher level cryptographic functions

### 7.1 'One Way Box'

This is used to encrypt a message allowing a given recipient to decrypt it without knowing the identity of the sender. This is useful because it reduces the metadata required to be carried with the message, obfuscating the identity of the sender from an eavesdropper.

Furthermore, it is useful for its one-way property. After the sender has created the message, they are not able to decrypt it themselves.  This makes it particularly useful for encrypting shards, because if all the shard messages are retained on the device of the secret-owner after being sent out, it is important that they cannot be recovered by the secret-owner themselves, as this would otherwise comprise of an extra copy of the secret being stored in a single location.

For transport systems which use append-only logs, this is absolutely essential, as it is not possible to remove the sent messages.

It works by using an ephemeral keypair for the sender, rather than their long-term key which is normally used. The public key ephemeral key is included with the ciphertext, and the private key is discarded after being used a single time, and never stored on disk.

![one way box](./assets/one-way-box.png)

Since shards should always be signed with the long term signing key of the secret-owner, and are never transmitted without this signature, using an ephemeral key for encryption does not introduce any doubt as to the identity of the secret owner.

- [`KeyBackupCrypto.oneWayBox` in API documentation](https://dark-crystal-java.gitlab.io/dark-crystal-key-backup-java-docs/dark-crystal-key-backup-crypto-java/org/magmacollective/darkcrystal/keybackup/crypto/KeyBackupCrypto.html#oneWayBox(byte%5B%5D,byte%5B%5D))
- [`KeyBackupCrypto.oneWayUnbox` in API documentation](https://dark-crystal-java.gitlab.io/dark-crystal-key-backup-java-docs/dark-crystal-key-backup-crypto-java/org/magmacollective/darkcrystal/keybackup/crypto/KeyBackupCrypto.html#oneWayUnbox(byte%5B%5D,org.whispersystems.curve25519.Curve25519KeyPair))
- [`KeyBackupCrypto.oneWayBox` in source code](https://gitlab.com/dark-crystal-java/dark-crystal-key-backup-crypto-java/-/blob/70f663ea2520c1a7aeb03fd2c0d1092ebcc028ed/src/main/java/org/magmacollective/darkcrystal/keybackup/crypto/KeyBackupCrypto.java#L272)
- [`KeyBackupCrypto.oneWayUnbox` in source code](https://gitlab.com/dark-crystal-java/dark-crystal-key-backup-crypto-java/-/blob/70f663ea2520c1a7aeb03fd2c0d1092ebcc028ed/src/main/java/org/magmacollective/darkcrystal/keybackup/crypto/KeyBackupCrypto.java#L287)

## 8. Shamir's secret sharing

Unlike more commonly used cryptographic primitives, there is little standardisation for threshold-based secret sharing algorithms. Although there does exist a standardised scheme 'SLIP39', this protocol does not completely adhere to it. Rather, we use a C implementation 'sss' authored by Daan Sprenkles.

The reasoning behind this choice is explained in [Choosing a threshold-based secret sharing algorithm](https://gitlab.com/dark-crystal-java/choice-of-secret-sharing-implementation).

'sss' has bindings for NodeJS, Go, Rust, and Webassembly, and includes a 'submodule' containing a secure random number generator.

Like many other secret sharing implementations, it uses a Lagrange interpolation on a 256 bit Galois field.

### 8.1 Authenticated encryption of secret

To ensure a uniformly random secret, as well as to enable integrity checking on recovery, the secret is not used directly in the secret sharing algorithm.  Rather, a random key is generated, the secret is encrypted with this key using the authenticated symmetric encryption technique described above, and this key is taken to be the secret in the secret sharing algorithm. The ciphertext is concatonated to each share.

![Secrets flow diagram](./assets/secrets-flow-diagram.png)

Share format: `share-index||share||ciphertext`

Although Daan Sprenkles' implementation includes this functionality, it restricts us to a 64 byte fixed length secret.  In some cases, this might make the share size unnecessarily long (which could be an issue, for example if the shares are to be encoded as QR codes). In order to give more flexibility, to allow longer secrets such as RSA keys, as well as compact secrets, our standard allows a variable length secret, with optional padding for cases where it is important to obfuscate the secrets length. 

- [`SecretSharingWrapper.share` in API documentation](https://dark-crystal-java.gitlab.io/dark-crystal-key-backup-java-docs/dark-crystal-secret-sharing-wrapper/org/magmacollective/darkcrystal/secretsharingwrapper/SecretSharingWrapper.html#share(byte%5B%5D,int,int))
- [`SecretSharingWrapper.combine` in API documentation](https://dark-crystal-java.gitlab.io/dark-crystal-key-backup-java-docs/dark-crystal-secret-sharing-wrapper/org/magmacollective/darkcrystal/secretsharingwrapper/SecretSharingWrapper.html#combine(java.util.List))
- [`SecretSharingWrapper.share` in source code](https://gitlab.com/dark-crystal-java/dark-crystal-secret-sharing-wrapper/-/blob/829bbe3b1a36598a33f076a8c146d0354d82109c/src/main/java/org/magmacollective/darkcrystal/secretsharingwrapper/SecretSharingWrapper.java#L115)
- [`SecretSharingWrapper.combine` in source code](https://gitlab.com/dark-crystal-java/dark-crystal-secret-sharing-wrapper/-/blob/829bbe3b1a36598a33f076a8c146d0354d82109c/src/main/java/org/magmacollective/darkcrystal/secretsharingwrapper/SecretSharingWrapper.java#L131)

### 8.2 Bit slicing

'sss' uses several techniques to protect against side channel attacks, including 'bit slicing'. These types of attacks rely on factors such as timing or even physical properties such as CPU temperature, to derive knowledge otherwise unavailable to an attacker. For example a custodian might be able to derive something about the secret by repeatedly trying a combination of their own share and several random shares, and observing the time the algorithm takes to run.

Bit slicing, involves taking the two dimensional array of shares and 'turning it on its side' when doing the interpolation computations, and then re-orienting the result appropriately afterwards. This makes it near impossible to determine knowledge about individual shares by observing physical factors relating to the computation.

### 8.3 Zero-padding of secret

![Zero padding of secret](./assets/zero-padding.png)

The technique of using authenticated encryption and using the key in the secret sharing algorithm allows us to have a variable length secret.  So any kind of key or data can be 'sharded' regardless of its size. However, it must be noted that the length of the secret can be determined by the length of a given share, so it is revealed to the custodian. In some situations this may be undesirable, for example when the secret is a password, or a particular kind of key with a characteristic length. A solution to this is to add padding, in order to make the secret have a constant length. For example a secret of length 32 could be padded with an additional 32 bytes, all of which are zero, to pad to a standard length of 64 bytes. 

- [`SecretSharingWrapper.zeroPad` method in API documentation](https://dark-crystal-java.gitlab.io/dark-crystal-key-backup-java-docs/dark-crystal-secret-sharing-wrapper/org/magmacollective/darkcrystal/secretsharingwrapper/SecretSharingWrapper.html#zeroPad(byte%5B%5D,int))
- [`SecretSharingWrapper.zeroPad` in source code](https://gitlab.com/dark-crystal-java/dark-crystal-secret-sharing-wrapper/-/blob/829bbe3b1a36598a33f076a8c146d0354d82109c/src/main/java/org/magmacollective/darkcrystal/secretsharingwrapper/SecretSharingWrapper.java#L221)

### 8.4 Obfuscation of the x-coordinate

![Random subset of shares](./assets/random-subset.png)

Shares are a collection of points on an array of parabolic curves, and contain both a 'share index', the x coordinate, which remains constant throughout the array, and the 'share value', an array of y coordinates. The share indexes are given a consecutive numbers for each share, so if we have 4 shares, they would have share indexes 1, 2, 3, and 4. This means that having a share gives some indication of the total number of shares. For example, if we have share number 3, we can infer that at least two other shares exist. To obfuscate this additional information, we generate a set of 255 shares and randomly select the desired amount of shares from this set. This means nothing can be inferred from knowing your own share value, other than that the number of shares is less than 255. 

This is achieved using a ['Durstenfeld schuffle'](https://en.wikipedia.org/wiki/Fisher%E2%80%93Yates_shuffle#Modern_method) algorithm, to shuffle the array of shares, but instead of completely shuffling the array, we take only the desired number of elements.

- [`SecretSharingWrapper.partialShuffleList` in API documentation](https://dark-crystal-java.gitlab.io/dark-crystal-key-backup-java-docs/dark-crystal-secret-sharing-wrapper/org/magmacollective/darkcrystal/secretsharingwrapper/SecretSharingWrapper.html#partialShuffleList(java.util.List,int))
- [`SecretSharingWrapper.partialShuffleList` in source code](https://gitlab.com/dark-crystal-java/dark-crystal-secret-sharing-wrapper/-/blob/829bbe3b1a36598a33f076a8c146d0354d82109c/src/main/java/org/magmacollective/darkcrystal/secretsharingwrapper/SecretSharingWrapper.java#L83)

### 8.5 Including a label with the secret

This is an optional feature to indicate the intended purpose of the secret, meaning that it is still useful if recovered 'out of context'. This will generally include a 'label' property which will be a human readable description, including, for example, the name of the application it was created with or the purpose. The label may also include application-specific data. 

This feature is important because, as Pamela Morgan makes clear in her book 'Crypto-asset inheritance planning', recovering a key is only half the story. For it be useful, we need to know what to do with it.  In some situations it makes less sense to include a descriptive label with the secret because the context in which the shares are stored gives us information about their intended use.  For example, if they are shares of an email encryption key used in an email client, the fact that they are stored with the other files relating to this application is probably enough context to determine what they are for. However, if the shares are to be printed on paper as mnemonics or QR codes, it is less obvious.

The amount of information included in a label depends on how critical it is that share size is kept small, which varies with different applications.

If this feature is adopted, the secret should be encoded as a protocol buffer with two properties, the secret itself, stored in binary form, and the label stored as a string.  Protocol buffers were chosen as the serialisation standard because it is widely adopted and implemented in many languages.

- [`SecretSharingWrapper.SecretWithLabel` in API documentation](https://dark-crystal-java.gitlab.io/dark-crystal-key-backup-java-docs/dark-crystal-secret-sharing-wrapper/org/magmacollective/darkcrystal/secretsharingwrapper/SecretSharingWrapper.SecretWithLabel.html)
- [`SecretSharingWrapper.decodeSecretWithLabel` in API documentation](https://dark-crystal-java.gitlab.io/dark-crystal-key-backup-java-docs/dark-crystal-secret-sharing-wrapper/org/magmacollective/darkcrystal/secretsharingwrapper/SecretSharingWrapper.html#decodeSecretWithLabel(byte%5B%5D))
- [`SecretSharingWrapper.SecretWithLabel` in source code](https://gitlab.com/dark-crystal-java/dark-crystal-secret-sharing-wrapper/-/blob/829bbe3b1a36598a33f076a8c146d0354d82109c/src/main/java/org/magmacollective/darkcrystal/secretsharingwrapper/SecretSharingWrapper.java#L38)
- [`SecretSharingWrapper.decodeSecretWithLabel` in source code](https://gitlab.com/dark-crystal-java/dark-crystal-secret-sharing-wrapper/-/blob/829bbe3b1a36598a33f076a8c146d0354d82109c/src/main/java/org/magmacollective/darkcrystal/secretsharingwrapper/SecretSharingWrapper.java#L71)

The protocol buffer schema for this message looks like this:
```
syntax = "proto2";

message Secret {
  required bytes secret = 1;
  optional string label = 2;
}
```

- [Protobuf schema for secret in source code](https://gitlab.com/dark-crystal-java/dark-crystal-secret-sharing-wrapper/-/blob/master/src/main/resources/SecretV1.proto)

## 9. Anatomy of a shard

A shard consists of the share component (a share of the key to the secret), the encrypted secret component, and a signature.  Including the share index, share, nonce, MAC, and signature mean the share length is 137 bytes, plus the length of the secret, or a standard length if zero-padding is used.

![Anatomy of a shard](./assets/anatomy-shard.png)

## 10. Descriptions of modules used in the reference implementation

- [Reference implementation API Documentation](https://dark-crystal-java.gitlab.io/dark-crystal-key-backup-java-docs/)

### 10.1 [dark-crystal-shamir-secret-sharing](https://gitlab.com/dark-crystal-java/dark-crystal-shamir-secret-sharing)

The secret sharing algorithm. This provides JNA bindings to the C library [dsprenkels/sss](https://github.com/dsprenkels/sss).

### 10.2 [dark-crystal-secret-sharing-wrapper](https://gitlab.com/dark-crystal-java/dark-crystal-secret-sharing-wrapper)

A wrapper around libSSS providing higher functionality specific to dark-crystal key backup, including:
- Variable length secrets with authentication
- Obfuscations of the x-coordinate (discussed below/above*)
- Optional zero-padding

### 10.3 [dark-crystal-key-backup-crypto-java](https://gitlab.com/dark-crystal-java/dark-crystal-key-backup-crypto-java) 

Contains cryptographic operations used for key backup and recovery.

### 10.4 [dark-crystal-key-backup-message-schemas-java](https://gitlab.com/dark-crystal-java/dark-crystal-key-backup-message-schemas-java)

Contains template schemas for Dark Crystal key backup messages, using protocol buffers, XML, and JSON.

## 11. Setup process

This section walks through the process making a distributed backup of a secret

### 11.1 Step 1 - Secret is combined with contextual metadata

![secret and label](./assets/dc_secret_label.png)

This step is optional and is to indicate the intended purpose of the secret, meaning that it is still useful if recovered 'out of context'. This means including a 'label' property which will be a human readable description, including, for example, the name of the application it is useful for. This may also include application-specific data.

The amount of information included here depends on how critical it is that share size is kept small.

- [`SecretSharingWrapper.SecretWithLabel` in API documentation](https://dark-crystal-java.gitlab.io/dark-crystal-key-backup-java-docs/dark-crystal-secret-sharing-wrapper/org/magmacollective/darkcrystal/secretsharingwrapper/SecretSharingWrapper.SecretWithLabel.html)
- [`SecretSharingWrapper.decodeSecretWithLabel` in API documentation](https://dark-crystal-java.gitlab.io/dark-crystal-key-backup-java-docs/dark-crystal-secret-sharing-wrapper/org/magmacollective/darkcrystal/secretsharingwrapper/SecretSharingWrapper.html#decodeSecretWithLabel(byte%5B%5D))

### 11.2 Step 2 - Encrypt data with symmetric key and Message Authentication Code added

![secret and label with key](./assets/dc_secret_label2.png)

The data is encrypted with a symmetric key and this key is taken to be the secret. Otherwise, the data itself is taken to be the secret. It needs to be noted that many implementations of secret sharing do this internally, and produce shares which are a concatonation of a key-share and the encrypted secret.

This means there is some duplication of data - a portion of each share is identical to the others. So in the case of particularly large secrets, it makes sense if the encrypted secret is stored only once in a place which is accessible to all share-holders (if the practicalities of the chosen transport/storage layer make this possible).

![secret label key mac](./assets/dc_secret_label3.png)

A `poly1305` MAC is used, which allows us to later verify that the secret has been correctly recovered.

- [`KeyBackupCrypto.secretBox` in API documentation](https://dark-crystal-java.gitlab.io/dark-crystal-key-backup-java-docs/dark-crystal-key-backup-crypto-java/org/magmacollective/darkcrystal/keybackup/crypto/KeyBackupCrypto.html#secretBox(byte%5B%5D,byte%5B%5D,byte%5B%5D))
- [`KeyBackupCrypto.secretBox` in source code](https://gitlab.com/dark-crystal-java/dark-crystal-key-backup-crypto-java/-/blob/70f663ea2520c1a7aeb03fd2c0d1092ebcc028ed/src/main/java/org/magmacollective/darkcrystal/keybackup/crypto/KeyBackupCrypto.java#L191)

### 11.3 Step 3 - Shards generated

![shards](./assets/dc_shards1.png)

Shards are generated using a secure threshold-based secret sharing algorithm, [dsprenkels/sss](https://github.com/dsprenkels/sss) 

### 11.4 Step 4 - Shards are signed

Each shard is signed by the owner of the secret using a keypair with an established public key (such as the same keypair used to sign other messages in the application).

Shards are appended with these signatures, meaning the complete shard format looks like this:

![shard format](./assets/shard-format.png)

This is to protect against shards being modified, maliciously or accidentally, and achieves the same goal as schemes known as 'Verifiable Secret Sharing'.

This is discussed in detail in our threat model report.

- [`SecretSharingWrapper.shareAndSign` in API documentation](https://dark-crystal-java.gitlab.io/dark-crystal-key-backup-java-docs/dark-crystal-secret-sharing-wrapper/org/magmacollective/darkcrystal/secretsharingwrapper/SecretSharingWrapper.html#shareAndSign(byte%5B%5D,int,int,java.security.PrivateKey))
- [`SecretSharingWrapper.shareAndSign` in source code](https://gitlab.com/dark-crystal-java/dark-crystal-secret-sharing-wrapper/-/blob/829bbe3b1a36598a33f076a8c146d0354d82109c/src/main/java/org/magmacollective/darkcrystal/secretsharingwrapper/SecretSharingWrapper.java#L164)
- [`SecretSharingWrapper.verifyAndCombine` in API documentation](https://dark-crystal-java.gitlab.io/dark-crystal-key-backup-java-docs/dark-crystal-secret-sharing-wrapper/org/magmacollective/darkcrystal/secretsharingwrapper/SecretSharingWrapper.html#verifyAndCombine(java.util.List,java.security.PublicKey))
- [`SecretSharingWrapper.VerifyAndCombine` in source code](https://gitlab.com/dark-crystal-java/dark-crystal-secret-sharing-wrapper/-/blob/829bbe3b1a36598a33f076a8c146d0354d82109c/src/main/java/org/magmacollective/darkcrystal/secretsharingwrapper/SecretSharingWrapper.java#L183)

### 11.5 Step 5 - Schema version number and timestamp added

A version number ensures backward compatibility with future versions of the protocol.

A timestamp allows secret-owner and custodian to keep track of what happened when.

- [Example of a timestamp being added to a message in the source code](https://gitlab.com/dark-crystal-java/dark-crystal-key-backup-message-schemas-java/-/blob/297c9417b4e08adee11ee6985e66509d7b7b55a5/src/main/java/org/magmacollective/darkcrystal/keybackup/messageschemas/Publish.java#L50)
- [Example of a version number being added to a message in the source code](https://gitlab.com/dark-crystal-java/dark-crystal-key-backup-message-schemas-java/-/blob/297c9417b4e08adee11ee6985e66509d7b7b55a5/src/main/java/org/magmacollective/darkcrystal/keybackup/messageschemas/Publish.java#L48)

### 11.6 Step 6 - Signed shards encrypted for each custodian

![shards](./assets/dc_shards2.png)

Shards are encrypted with the public key of each custodian. The unencrypted shards are removed from memory. 

'One-way box' is used to ensure the custodian can read the message, but the secret-owner cannot read it themselves.

This is inspired by [private-box](https://github.com/auditdrivencrypto/private-box), see [this note in the design document](https://github.com/auditdrivencrypto/private-box/blob/master/design.md#one-way-box).  We could also just use private-box itself for this, but this method is simpler and gives us smaller shards.

- [`KeyBackupCrypto.oneWayBox` in API documentation](https://dark-crystal-java.gitlab.io/dark-crystal-key-backup-java-docs/dark-crystal-key-backup-crypto-java/org/magmacollective/darkcrystal/keybackup/crypto/KeyBackupCrypto.html#oneWayBox(byte%5B%5D,byte%5B%5D))
- [`KeyBackupCrypto.oneWayBox` in source code](https://gitlab.com/dark-crystal-java/dark-crystal-key-backup-crypto-java/-/blob/70f663ea2520c1a7aeb03fd2c0d1092ebcc028ed/src/main/java/org/magmacollective/darkcrystal/keybackup/crypto/KeyBackupCrypto.java#L272)
### 11.7 Step 7 - Transmission

Each encrypted shard is packed together with some metadata into a message, transmitted to the custodian, and a local (encrypted) copy is retained.

Additionally, a '`root`' message is created which contains some metadata describing the secret. This message is retained locally and serves as a record of the backup.

Details of these messages, as well as of the system of requesting, responding, and forwarding shards, are explained in the [message schemas section](https://gitlab.com/dark-crystal-java/dark-crystal-key-backup-message-schemas-java)

- [Message Schemas API documentation](https://dark-crystal-java.gitlab.io/dark-crystal-key-backup-java-docs/dark-crystal-key-backup-message-schemas/org/magmacollective/darkcrystal/keybackup/messageschemas/package-summary.html)
- [Message schemas source code](https://gitlab.com/dark-crystal-java/dark-crystal-key-backup-message-schemas-java)

## 12. Recovery process

This section walks through the process of recovering a secret from a distributed backup

### 12.1 Step 1 - New identity

Upon loss of data, the secret owner establishes a new account, giving them a new identity on the system. This step depends a lot on the transport mechanism used, but will generally involve generating a new keypair.

### 12.2 Step 2 - Contact custodians

The secret owner contacts the custodians 'out of band' to confirm that the new identity belongs to them. That is, it is assumed that there is the possibility of some personal contact to convince the custodians that the new identity is really the secret owner.  For example this might involve a phone call saying "hey, its me!".

Due to the threshold nature of the scheme there is a degree of tolerance to some custodians being unavailable or uncooperative.

The user interface should be designed to encourage the custodians to get the secret owner to confirm their new key out of band.  If on a phone call, rather than trying to confirm some characters from the key itself, it can be much easier to confirm some dictionary words derived from the key.

### 12.3 Step 3 - Return shards

Each custodian decrypts the shard they are holding with their personal keypair, and re-encrypts it to the public key of the new account. It is then sent to the new account of the secret owner.

The nature peer to peer protocols make it difficult to delete data. If this is the case with the transport mechanism you are using, we recommend adding a second layer of encryption using an ephemeral keypair. This is a single-use keypair which can later be deleted to effectively delete these messages from the system.  This will be explained in more detail in a separate document.

### 12.4 Step 3 - Decrypt shards

The secret owner decrypts the shards they receive, using `oneWayUnbox` (described above).

- [`KeyBackupCrypto.oneWayUnbox` in API documentation](https://dark-crystal-java.gitlab.io/dark-crystal-key-backup-java-docs/dark-crystal-key-backup-crypto-java/org/magmacollective/darkcrystal/keybackup/crypto/KeyBackupCrypto.html#oneWayUnbox(byte%5B%5D,org.whispersystems.curve25519.Curve25519KeyPair))
- [`KeyBackupCrypto.oneWayUnbox` in source code](https://gitlab.com/dark-crystal-java/dark-crystal-key-backup-crypto-java/-/blob/70f663ea2520c1a7aeb03fd2c0d1092ebcc028ed/src/main/java/org/magmacollective/darkcrystal/keybackup/crypto/KeyBackupCrypto.java#L287)

### 12.5 Step 4 - Validate shards

![validated shards](./assets/validated-shards-sm.png)

The signature of each shard is validated with the original public key, proving that the returned shards are identical to those sent out. 

In the case that the shard could not be validated, the shard data can be retrieved anyway using `detachMessage`.

- [`SecretSharingWrapper.VerifyAndCombine` in API documentation](https://dark-crystal-java.gitlab.io/dark-crystal-key-backup-java-docs/dark-crystal-secret-sharing-wrapper/org/magmacollective/darkcrystal/secretsharingwrapper/SecretSharingWrapper.html#verifyAndCombine(java.util.List,java.security.PublicKey))
- [`SecretSharingWrapper.VerifyAndCombine` in source code](https://gitlab.com/dark-crystal-java/dark-crystal-secret-sharing-wrapper/-/blob/829bbe3b1a36598a33f076a8c146d0354d82109c/src/main/java/org/magmacollective/darkcrystal/secretsharingwrapper/SecretSharingWrapper.java#L183)
- [`KeyBackupCrypto.detachMessage` in API documentation](https://dark-crystal-java.gitlab.io/dark-crystal-key-backup-java-docs/dark-crystal-key-backup-crypto-java/org/magmacollective/darkcrystal/keybackup/crypto/EdDSA.html#detachMessage(byte%5B%5D))
-[`KeyBackupCrypto.detachMessage` in source code](https://gitlab.com/dark-crystal-java/dark-crystal-key-backup-crypto-java/-/blob/70f663ea2520c1a7aeb03fd2c0d1092ebcc028ed/src/main/java/org/magmacollective/darkcrystal/keybackup/crypto/EdDSA.java#L69)

### 12.6 Step 5 - Secret recovery

![recovery](./assets/recovery-sm.png)

The shards are combined to recover the secret.

- [`SecretSharingWrapper.combine` in API documentation](https://dark-crystal-java.gitlab.io/dark-crystal-key-backup-java-docs/dark-crystal-secret-sharing-wrapper/org/magmacollective/darkcrystal/secretsharingwrapper/SecretSharingWrapper.html#combine(java.util.List))
- [`SecretSharingWrapper.combine` in source code](https://gitlab.com/dark-crystal-java/dark-crystal-secret-sharing-wrapper/-/blob/829bbe3b1a36598a33f076a8c146d0354d82109c/src/main/java/org/magmacollective/darkcrystal/secretsharingwrapper/SecretSharingWrapper.java#L131)

### 12.7 Step 6 - Validate secret

![recovery successful](./assets/recover-success-sm.png)

The MAC is used to establish that recovery was successful.  This means we can be sure the combining process worked as planned and offers some protection against tampering.

- [`KeyBackupCrypto.secretUnbox` in source code](https://gitlab.com/dark-crystal-java/dark-crystal-key-backup-crypto-java/-/blob/70f663ea2520c1a7aeb03fd2c0d1092ebcc028ed/src/main/java/org/magmacollective/darkcrystal/keybackup/crypto/KeyBackupCrypto.java#L241)

### 12.8 Step 7 - Decrypt secret

![secret and label](./assets/dc_secret_label.png)

Finally the secret is restored, along with a descriptive label.

- [`KeyBackupCrypto.decrypt` in API documentation](https://dark-crystal-java.gitlab.io/dark-crystal-key-backup-java-docs/dark-crystal-key-backup-crypto-java/org/magmacollective/darkcrystal/keybackup/crypto/KeyBackupCrypto.html#decrypt(byte%5B%5D,byte%5B%5D))
- [`KeyBackupCrypto.decrypt` in source code](https://gitlab.com/dark-crystal-java/dark-crystal-key-backup-crypto-java/-/blob/70f663ea2520c1a7aeb03fd2c0d1092ebcc028ed/src/main/java/org/magmacollective/darkcrystal/keybackup/crypto/KeyBackupCrypto.java#L176)

### 12.9 Step 8 - Recover original account

Depending on whether the lost account might have been compromised, it may be appropriate to abandon the new identity and continue to use the old one. If this is not the case, the key can at least be used to recover data encrypted to it.

In order to avoid confusion between the new 'temporary' identity, and the original one being restored, it may make sense, at the user-interface level, to restrict what a peer is able to with the application whilst they are in the process of retrieving shards from custodians.  For example making clear that they are currently in 'recovery mode' and are not able to use the normal features of the application until either their original identity is restored, or they create a 'normal' new account.

